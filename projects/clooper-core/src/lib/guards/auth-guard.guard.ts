import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthServiceService } from '../services/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  LoginStatus: boolean = false;

  constructor(private auth: AuthServiceService, private router: Router) {
    this.ListentoLoginStatus();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.LoginStatus) {
      return true;
    }

    else {
      this.router.navigateByUrl('/');
      return false;
    }
  }

  ListentoLoginStatus() {
    this.auth.loginStatusObs$.subscribe(res => {
      this.LoginStatus = res;
    });
  }

}
