export interface signInModel {
    email: string
    password: string
}

export interface createModel {
    email: string
    password: string
    first_name: string
    last_name: string
    accepts_term?: boolean 
}