import { NgModule } from '@angular/core';
import { ClooperCommonComponent } from './clooper-common.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ButtonComponent } from './components/button/button.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { TogglePasswordVisibilityDirective } from './directives/toggle-password-visibility.directive';
import { FullNamePipe } from './pipes/full-name.pipe';



@NgModule({
  declarations: [
    ClooperCommonComponent,
    PaginationComponent,
    ButtonComponent,
    AccordionComponent,
    TogglePasswordVisibilityDirective,
    FullNamePipe
  ],
  imports: [
  ],
  exports: [
    ClooperCommonComponent,
    PaginationComponent,
    ButtonComponent,
    AccordionComponent,
    TogglePasswordVisibilityDirective,
    FullNamePipe
  ]
})
export class ClooperCommonModule { }
