import { Inject, Injectable, InjectionToken } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

export const API_ENDPOINT  = new InjectionToken<string>('API_ENDPOINT ');

@Injectable()
export class UrlInterceptorInterceptor implements HttpInterceptor {
  constructor(@Inject(API_ENDPOINT ) private API_ENDPOINT : string) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({ url: this.prepareUrl(req.url) });
    return next.handle(req);
  }

  private isAbsoluteUrl(url: string): boolean {
    const absolutePattern =  new RegExp("^(http|https)://", "i");
    return absolutePattern.test(url);
  }

  private prepareUrl(url: string): string {
    url = this.isAbsoluteUrl(url) ? url : this.API_ENDPOINT  + '/' + url;
    return url.replace(/([^:]\/)\/+/g, '$1');
  }
}
