import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ClooperCommonModule } from 'clooper-common';
import { ClooperCoreModule } from 'clooper-core';
import { API_ENDPOINT, UrlInterceptorInterceptor } from 'projects/clooper-core/src/lib/interceptors/url-interceptor.interceptor';
import { environment } from 'projects/savings/src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    ClooperCommonModule,
    ClooperCoreModule
  ],
  providers: [
    { provide: API_ENDPOINT, useValue: environment.API_ENDPOINT },
    { provide: HTTP_INTERCEPTORS, useClass: UrlInterceptorInterceptor, multi: true, deps: [API_ENDPOINT] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
