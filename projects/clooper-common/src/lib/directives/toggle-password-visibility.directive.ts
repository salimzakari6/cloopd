import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[libTogglePasswordVisibility]'
})
export class TogglePasswordVisibilityDirective {

  private showPassword = false;

  constructor(private element: ElementRef) {
    const input = this.element.nativeElement.parentNodeNode;
    const span = document.createElement('span');
    span.innerHTML = 'show';
    span.addEventListener('click', () => {
      this.toggle(span);
    });
    input.appendChild(span);
  }

  toggle(span: HTMLElement) {
    if (this.showPassword) {
      this.element.nativeElement.setAttribute('type', 'text');
      span.innerHTML = 'hide';
    } else {
      this.element.nativeElement.setAttribute('type', 'password');
      span.innerHTML = 'show';
    }
  }
}
