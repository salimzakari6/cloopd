import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { signInModel } from '../models/model';


@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  private loginStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loginStatusObs$ = this.loginStatus.asObservable()


  

  constructor(
    private http: HttpClient,
  ) { }


  signin(payload: signInModel): Observable<any> {
    return this.http.post<signInModel>(`/auth`, payload)
      .pipe(
        tap(() => this.setLoginStatus(true)),
        catchError(err => {
          this.setLoginStatus(false)
          return this.errorHandler(err)
        })
      )
  }

  setLoginStatus(status: boolean){
    this.loginStatus.next(status)
  }



  private errorHandler(err: any): Observable<never> {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error?.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body?.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }

}
