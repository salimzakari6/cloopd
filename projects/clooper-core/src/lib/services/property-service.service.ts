import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PropertyServiceService {


  constructor(
    private http: HttpClient,
  ) { }
    
  properties$ = this.http.get<any>('/properties')
  

}
