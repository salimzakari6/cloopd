import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DealServiceService {

  constructor(
    private http: HttpClient,
  ) { }
    
  deals$ = this.http.get<any>('/deals')
  

}
